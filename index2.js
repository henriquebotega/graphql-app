const express = require('express')
const app = express();

const expressGraphql = require("express-graphql");
const { buildSchema } = require("graphql");

const schema = buildSchema(`
  type Produto {
    id: ID
    titulo: String
    descricao: String
    valor: Int
  }

  type Query {
    produto(id: ID!): Produto
    produtos: [Produto]
  }

  type Mutation {
    criarProduto(titulo: String!, descricao: String, valor: Int!): Produto
  }
`);

const providers = {
    produtos: []
};

let id = 0;

const resolvers = {
    produto({ id }) {
        return providers.produtos.find(item => item.id === Number(id));
    },
    produtos() {
        return providers.produtos;
    },
    criarProduto({ titulo, descricao, valor }) {
        const item = {
            id: id++,
            titulo,
            descricao,
            valor
        };

        providers.produtos.push(item);

        return item;
    }
};

app.use("/graphql",
    expressGraphql({
        schema,
        rootValue: resolvers,
        graphiql: true
    })
);

app.listen(3000);

/*
// CRIA UM PRODUTO
mutation {
    criarProduto(
        titulo: "Higo",
        descricao: "https://www.google.com",
        valor: 23
    ) { id }
}

// LISTA TODOS OS PRODUTOS
{
    produtos {
        titulo
        valor
    }
}

// FILTRA UM PRODUTO
query {
    produto(id:1) {
        titulo
    }
}
*/
